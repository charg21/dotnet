# Concurrent Dictionary 분석 ( 작성중 )


해시 테이블 구현체입니다.

해시 충돌이 발생할 경우 체이닝 방식을 사용합니다.

동시성을 보장하기 위해 락프리 알고리즘이 아닌 버킷별로 락을 거는 세밀한 동기화( fine grained synchronization ) 방식으로 구현되어 있습니다.


## 필드

```csharp
/// <summary>
/// 내부 해시 테이블 구현체
/// </summary>
private volatile Tables _tables;

/// <summary>
/// 리사이즈 전 버킷별 락의 최대 갯수
/// </summary>
private int _budget;

```

## 동작

### 삽입 및 갱신, AddOrUpdate()

버킷별 잠금을 획득 후 추가한다.

```csharp
public TValue AddOrUpdate( TKey key, Func< TKey, TValue > addValueFactory, .... )
{
		// 중간에 테이블이 변경되는것을 확인하기 위해 사본
		Tables tables = _tables;
		
		// 해시 획득( 삽입할 버킷의 인덱스 )
		int hashcode = GetHashCode( comparer, key );

		while( true )
		{
				// Get 시도 
				if ( TryGetValueInternal( tables, key, hashcode, out TValue? oldValue ) )		
				{			
						// 이미 있던 경우 업데이트 하고 성공한다면 바로 리턴
						if ( TryUpdateInternal( tables, key, hashcode, newValue, oldValue ) )
							return newValue;
				}
				else // Get 실패시
				{
						// 삽입 시도
						if ( TryAddInternal( tables, key, hashcode... )
							return value; /// 성공시 바로 리턴
				}
		}

		/// 위에 과정이 실패한 상황에서 테이블이 변경( 리사이즈 )되었다면
		if ( table != _tbale )
		{
			//  해시코드와 테이블 사본을 갱신
			comparer = tables._comparer;
      		hashcode = GetHashCode( comparer, key );
		}
}

2. 삭제
```

### 리해시 or 리사이즈 GrowTable()

원소를 추가하는 과정이 트리거가 되어, 리해시 또는 리사이즈가 필요한 경우 while 루프 내에서 GrowTable() 메소드를 호출한다.

```csharp
private void GrowTable( Tables tables, bool resizeDesired, bool forceRehashIfNonRandomized)
{
	// 0번 락을 획득한다
	AcquireFirstLock( ref locksAcquired );

	// 0번 락을 획득한 상태에서 테이블 변경되었는지 여부를 체크
	// 테이블이 변겨된 경우( = 다른 스레드에서 이미 원소 추가하면서 GrowTable()을 호출한경우 ) 리턴
	if ( tables != _tables )
		return;
	
	// 리사이즈가 필요한 경우
	if ( resizeDesired )
	{
		// 적절한 사이즈 구하고...
		
		///... 생략
	}
	
	// 기존 테이블의 0번락을 제외한 락을 모두 획득
	AcquirePostFirstLock( tables, ref locksAcquired );

	// 신규 테이블 생성 및 기존 테이블 내용 신규 테이블에 복사
	// .. 생략

	// 최종적으로 테이블 반영
	_tables = nweTable;

	// 잠금들을 푼다.
	ReleaseLocks( locksAcquired );
}
```